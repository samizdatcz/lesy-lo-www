var map = L.map('map', {
  zoom: 7,
  center: [49.819957, 15.47491],
  minZoom: 7,
  maxZoom: 10
});

var info = L.control();
info.options.position = 'bottomright';
info.onAdd = function (map) {
  this._div = L.DomUtil.create('div', 'info');
  this.update();
  return this._div;
};

info.update = function (props) {
  this._div.innerHTML = (props ? '<values>Lesní oblast <b>' + props.NAZEV_PLO
  + '</b><br><div style="background:#a6cee3; width: 11px; height: 13px; position: absolute"></div>&nbsp;&nbsp;&nbsp;&nbsp;Bříza ' + props.bříza
  + ' ha<br><div style="background:#1f78b4; width: 11px; height: 13px; position: absolute"></div>&nbsp;&nbsp;&nbsp;&nbsp;Dub ' + props.dub
  + ' ha<br><div style="background:#b2df8a; width: 11px; height: 13px; position: absolute"></div>&nbsp;&nbsp;&nbsp;&nbsp;Buk ' + props.buk
  + ' ha<br><div style="background:#33a02c; width: 11px; height: 13px; position: absolute"></div>&nbsp;&nbsp;&nbsp;&nbsp;Borovice ' + props.borovice
  + ' ha<br><div style="background:#fb9a99; width: 11px; height: 13px; position: absolute"></div>&nbsp;&nbsp;&nbsp;&nbsp;Smrk ' + props.smrk_ztepilý
  + ' ha<br><div style="background:#e31a1c; width: 11px; height: 13px; position: absolute"></div>&nbsp;&nbsp;&nbsp;&nbsp;Modřín ' + props.modřín + ' ha'
    : 'Najetím myši vyberte lesní oblast.');
};

var scale = d3.scale.linear().domain([4612,241073]).range([10,30]);

var highlightStyle = {
  color: '#2ca25f',
  fillColor: '#a1d99b',
  weight: 2
};
var lowStyle = {
  color: '#31a354',
  fillColor: '#a1d99b',
  weight: 1
};

$.get('lesy-lo.topo.json', function(data){
  var dataJson = topojson.feature(data, data.objects.lesy_lo_geo);
  var poly = L.geoJson(dataJson, {
    style: function(feature){
      return {color: '#31a354', fillColor: '#a1d99b', weight: 1}
    },
    onEachFeature: function(feature, layer) {
      layer.on('mouseover', function() {
        info.update(feature.properties)
        layer.setStyle(highlightStyle);
      });
      layer.on('mouseout', function(){
        info.update(feature.properties);
        layer.setStyle(lowStyle);
      })
    }
  }).addTo(map);

  dataJson.features.forEach(function(feature) {
    L.piechartMarker(
      L.latLng([feature.properties.Y, feature.properties.X]),
      {
        radius: scale(feature.properties.celkem),
        data: [
          { name: 'Bříza', value: feature.properties.bříza, style: { fillStyle: '#a6cee3'} },
          { name: 'Dub', value: feature.properties.dub, style: { fillStyle: '#1f78b4'} },
          { name: 'Buk', value: feature.properties.buk, style: { fillStyle: '#b2df8a'} },
          { name: 'Borovice', value: feature.properties.borovice, style: { fillStyle: '#33a02c'} },
          { name: 'Smrk', value: feature.properties.smrk_ztepilý, style: { fillStyle: '#fb9a99'} },
          { name: 'Modřín', value: feature.properties.modřín, style: { fillStyle: '#e31a1c'} },
        ]
      }
    ).addTo(map);
  })
//      poly.on('mouseover', function(evt){info.update(evt);}).on('mouseout', function(evt){info.update();})
});

map.addControl(info)